﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPool : MonoBehaviour {

    public int coinPoolSize = 5;
    public GameObject coinPrefab;
    public Vector2 objectCoinPosition = new Vector2(-10f, -25f);
    public float coinRate = 4f;
    public float coinMin = -1f;
    public float coinMax = 3.5f;

    private GameObject[] coins;
    private float timeSinceLastSpawned;
    private float spawnXPosition = 20f;
    private int currentCoin = 0;

	// Use this for initialization
	void Start () {
        coins = new GameObject[coinPoolSize];
        for (int i = 0; i < coinPoolSize; i++)
        {
            coins[i] = (GameObject)Instantiate(coinPrefab, objectCoinPosition, Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
        timeSinceLastSpawned += Time.deltaTime;

        if (GameControl.instance.gameOver == false && timeSinceLastSpawned >= coinRate)
        {
            timeSinceLastSpawned = 0;
            float spawnYPosition = Random.Range(coinMin, coinMax);
            coins[currentCoin].transform.position = new Vector2(spawnXPosition, spawnYPosition);
            currentCoin++;
            if (currentCoin >= coinPoolSize)
            {
                currentCoin = 0;
            }
        }
	}
}
